const inputEl = document.getElementById("randomPassword");
const selectEl = document.getElementById("passwordLength");
const generatePasswordBtn = document.getElementById("randomPasswordBtn");


generatePasswordBtn.addEventListener("click", function()  {
    inputEl.value = "...";
    let allChars = "0987654321abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()[]{}";
    let password = "";
    if(selectEl.value == '') {
        alert("Please select password length");
        return;
    }
    for(let i=0; i< selectEl.value; i++) {
        let random = Math.floor(Math.random() * allChars.length);

        password += allChars.substring(random, random + 1);
    }
    inputEl.value = password
});